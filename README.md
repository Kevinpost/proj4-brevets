# README #
# proj4-Brevets
## **Author Information**

* Name:  Initial version by M Young; Docker version added by R Durairajan; revised by Kevin Post

* Contact address: Kpost7@uoregon.edu

## **Program description**

ACP brevets are a bike race with strict rules.   (https://rusa.org/pages/rulesForRiders)
At checkpoints, or "controls" along the path of the race, riders have a certain time window within which they must pass that point, with a minimum and maximum time listed.  A user will select a time and date which the race starts, then fill in control point distances, at which point the open and close times automatically populate with the correct date and time.   The overall behavior of this project is to re-create the logic of this calculator:  (https://rusa.org/octime_acp.html)


## **Program structure**
This is a web page hosted in a flask server, using AJAX for interactions between the user and the server.  Calc.html contains front-end logic that the user will see and interact with.  Flask_brevets.py is the back-end logic that the front end will interact with directly using AJAX & Jquery.  The module that actually calculates the open and close times is called acp_times.py.  The open and close times are calculated by two separate functions, which themselves call some simple helper functions.  

## **The algorithm**
Note: I have implemented the OFFICIAL ACP rules, with relaxed control points at <=60km.  (More on that below)

Brevets have pre-defined lengths: They must be 200, 300, 400, 600, or 1000 km.  Note that there may be control points up to 20% past the original end of a race (IE up to 1200 km control point for a 1000 km brevet is alright), but the open and close times for any control point past the brevet length will be equal to the open and close time at the brevet's length.  IE:  The open and close times for 1100 km in a 1000 km brevet are the same as the open and close times at the 1000 km mark.

The open and close times of a given control location (in km) are determined by the minimum and maximum speeds in the table at the to top of https://rusa.org/pages/acp-brevet-control-times-calculator.
Note that maximum speeds are used to calculate open times, and minimum speeds are used to calculate the close times.

OPEN TIME:
To illustrate how this works, I will use a specific example from the above link.  
Opening time for 890 km on a 1000km brevet:
The maximum speed for the 0-200km range is 34 km/hr, the max speed for 200-400km range is 32 km... etc.
Therefore the opening time is the start time shifted by:
(200/34 + 200/32 + 200/30 + XXXXXXX/28 )  hours

We see these values are taken directly from the table.  But what about the XXXXX in the above formula?  Well, we see that the first 600 miles are accounted for, 890 is between 600 and 1000, but only 290 of those miles are in this range.  Therefore, the XXXXXX is 290.  For a final answer of 29HR09 minutes.

CLOSE TIME:
This is calculated the same as the open time, except with its own special set of rules.  Reminder:  Use the minimum speeds rather than the maximum speeds.  Notice that the minimum speed is the same all the way up to 600 km, so it is simpler than open times in some ways.  However, as I said it has its own set of specific rules laid out as follows:

Special rules:
-Official close times for end of race:
In case of conflict between the algorithm I just described, the official "end times" of the race ultimately decide the close time for any control point at or after the end of the brevet's length.  They are as follows:
(HH:mm) 13:30 for 200 km, 20:00 for 300 km, 27:00 for 400 km, 40:00 for 600 km, and 75:00 for 1000 km

-When control location within the first 60 km:
The close time for a control within the first 60 km is based on 20km/hr plus 1 hour.


## Testing

Program includes a suite of nose tests.  To run tests, navigate to "brevets" directory and type "nosetests".
