"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import logging
from time import sleep

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
###
# Pages
###

times_list = []

@app.route("/")
@app.route("/index")
def index():
    global times_list
    times_list = []
    db.tododb.drop()
    #FIXME: EMPTY DATABASE
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############


@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    date = request.args.get('date', type = str)
    time = request.args.get('time', type = str)
    dist_km = request.args.get('dist_km', type = int)
    date_time_string = date + ' ' + time + ':00'
    date_time_arrow = arrow.get(date_time_string, 'YYYY-MM-DD HH:mm:ss')

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    mesg = "EMPTY"
    if (km > (dist_km*1.2)):
        mesg = "1 OR MORE INPUT ERRORS: km > (dist_km*1.2)"
    elif (km < 0):
        mesg = "1 OR MORE INPUT ERRORS: km < 0"

    open_time = acp_times.open_time(km, dist_km, date_time_arrow.isoformat())
    close_time = acp_times.close_time(km, dist_km, date_time_arrow.isoformat())

    result = {"open": open_time, "close": close_time, "message": mesg}
    close_time_str = str(close_time)
    open_time_str = str(open_time)
    times_list.append("BREVET LENGTH: <" + str(dist_km) + "km>___START DATE: <" + date + ">___START TIME: <" + time + ">Control point: <" + str(km) + "km>___" + "OPEN: <" + open_time_str[:16] + ">___ " + "CLOSE: >" + close_time_str[:16])
    #print(times_list)
    return flask.jsonify(result=result)


@app.route('/_subby')
def _submit():
    for item in times_list:
        item_doc = {'entry': item}
        db.tododb.insert_one(item_doc)
        #print("submitted " + item)
    #print("INSIDE SUBMIT")
    return ("empty")

@app.route('/todo.html')
def todo():
    return render_template('todo.html')

@app.route('/display.html', methods=['GET'])
def _display():
    #print("INSIDE DISPLAY")
    _items = db.tododb.find()
    items = [item for item in _items]
    return flask.render_template('display.html', items=items)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
